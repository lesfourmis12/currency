import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

//libs
import { DxDataGridModule, DxChartModule, DxDateBoxModule, DxSelectBoxModule } from 'devextreme-angular';

//comps
import { AppComponent } from './app.component';
import { StocksInfoComponent } from '../components/stocks-info/stocks-info.component';
import { NotifierComponent } from '../components/notifier/notifier.component';
import { CandlesComponent } from '../components/candles/candles.component';
import { SocketComponent } from '../components/socket/socket.component';

//services
import { StocksService } from 'src/services/stocks.service';
import { NotifierService } from 'src/services/notifier.service';
import { WebSocketService } from 'src/services/web-socket.service';

//interceptors
import { ApiInterceptor } from '../interceptors/api.interceptor';
import { ErrorInterceptor } from '../interceptors/error.interceptor';

// modules
import { WebSocketModule } from '../modules/web-socket/web-socket.module';

import { apiToken } from '../env';

@NgModule({
  declarations: [
    AppComponent,
    StocksInfoComponent,
    NotifierComponent,
    CandlesComponent,
    SocketComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    HttpClientModule,
    DxDataGridModule,
    DxDateBoxModule,
    DxSelectBoxModule,
    DxChartModule,

    WebSocketModule.config({
      url: `wss://ws.finnhub.io?token=${apiToken}`
    })
  ],
  providers: [
    StocksService,
    NotifierService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
