import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { LoadOptions } from 'devextreme/data/load_options';

import { StocksService, Symbol } from '../../services/stocks.service';

@Component({
  selector: 'app-test-stocks-info',
  templateUrl: './stocks-info.component.html',
  styleUrls: ['./stocks-info.component.scss'],
})
export class StocksInfoComponent implements OnInit {
  public stocks: Symbol[] = [];
  public loading: boolean = false;
  public details: CustomStore;

  constructor(private _stocksService: StocksService, private _router: Router) {
    this.details = new CustomStore({
      key: 'ID',
      // loadMode: 'raw',
      load: (loadOpts: LoadOptions) => {
        console.log(loadOpts);
        // вот тут не понял, как передать в loadOpts параметры какие-нибудь
        // const symbol = 'APPL';
        const symbol = loadOpts.userData['symbol'];
        return this._stocksService.getCompanyProfile(symbol.split('/')[0])
          .catch(() => { throw 'Data loading error' });
      }
    });
  }

  ngOnInit() {
    this.takeStocks();
  }

  public async takeStocks() {
    this.loading = true;
    this.stocks = await this._stocksService.getStocks();
    console.log('main table get stocks', this.stocks)
    this.loading = false;
  }

  public onRowClick(data: Symbol) {
    this._router.navigate([], { queryParams: { symbol: data.symbol } });
  }
}
