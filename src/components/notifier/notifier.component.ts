import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotifierService, Alert } from '../../services/notifier.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-test-notifier',
  templateUrl: './notifier.component.html',
  styleUrls: ['./notifier.component.scss'],
})
export class NotifierComponent implements OnInit {
  public alerts: Alert[] = [];
  private _sub: Subscription = null;

  constructor(private _notifyService: NotifierService) {}

  ngOnInit() {
    this._sub = this._notifyService.alerts$.subscribe({
      next: (alerts: Alert[]) => {
        this.alerts = alerts;
      },
    });
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }
}
