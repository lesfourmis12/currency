import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';

import { WebSocketService } from '../../services/web-socket.service';

export interface IMessage {
  id: number;
  text: string;
}

@Component({
  selector: 'app-test-socket',
  templateUrl: './socket.component.html',
  styleUrls: ['./socket.component.scss'],
})
export class SocketComponent implements OnInit {
  public data: { lastPrice: number; time: Date }[] = [];

  constructor(
    private _route: ActivatedRoute,
    private _wsService: WebSocketService
  ) {}

  ngOnInit(): void {
    // Router subscription
    const routeSub = this._route.queryParams
      .pipe(
        filter((queryParams) => queryParams.symbol),
        map((queryParams) => queryParams.symbol)
      )
      .subscribe({
        next: (symbol) => {
          this.data = [];
          this._wsService.send('trade', { symbol });
        },
      });

    // trade example
    // { p: 125.513
    // s: "OANDA:USB05Y_USD"
    // t: 1605644052861
    // v: 0}
    this._wsService
      .on<any[]>('trade')
      .pipe(
        map((data: any[]) => {
          return data.map(({ p: lastPrice, t: time }) => ({
            lastPrice,
            time: new Date(time / 1000),
          }));
        })
      )
      .subscribe({
        next: (res) => {
          let newArr = [...this.data];
          newArr.push(...res);
          newArr = newArr.slice(-10);
          this.data = newArr;
          console.log(res);
        },
      });
  }
}
