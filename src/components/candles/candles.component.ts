import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { StocksService, Resolution } from '../../services/stocks.service';

@Component({
  selector: 'app-test-candles',
  templateUrl: './candles.component.html',
  styleUrls: ['./candles.component.scss'],
  providers: [],
})
export class CandlesComponent implements OnInit, OnDestroy {
  private _subs: Subscription[] = [];

  public candles: [] = [];
  public loading: boolean = false;

  public resList = this._stocksService.getResList();

  params$: BehaviorSubject<{
    symbol: string;
    resolution: Resolution;
    from: Date;
    to: Date;
  }> = new BehaviorSubject({
    symbol: '',
    resolution: null,
    from: null,
    to: null,
  });

  constructor(
    private _route: ActivatedRoute,
    private _stocksService: StocksService
  ) {}

  ngOnInit() {
    // Router subscription
    const routeSub = this._route.queryParams
      .pipe(
        filter((queryParams) => queryParams.symbol),
        map((queryParams) => queryParams.symbol)
      )
      .subscribe({
        next: (symbol) => {
          this.params$.next({
            ...this.params$.value,
            symbol,
          });
        },
      });

    // Params subscription
    const paramsSub = this.params$
      .pipe(
        filter(({ symbol, resolution, from, to }) => {
          return symbol && resolution && !!from && !!to;
        })
      )
      .subscribe({
        next: (params) => {
          console.log('try fetch with params', params);
          this.takeCandles(params);
        },
      });

    this._subs.push(routeSub, paramsSub);
  }

  ngOnDestroy() {
    this._subs.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  async takeCandles(params: any) {
    this.loading = true;

    //fetch
    this.candles = await this._stocksService.getCandles(
      params.symbol,
      params.resolution,
      params.from,
      params.to
    );

    console.log('chart get candles', this.candles);
    this.loading = false;
  }

  public onDateFromChanged(timeFrom) {
    this.params$.next({
      ...this.params$.value,
      from: new Date(timeFrom),
    });
  }

  public onDateToChanged(timeTo) {
    this.params$.next({
      ...this.params$.value,
      to: new Date(timeTo),
    });
  }

  public onResChanged(res) {
    this.params$.next({
      ...this.params$.value,
      resolution: res,
    });
  }

  public customizeTooltip(arg) {
    console.log(arg);
    return {
      text:
        'Open: ' +
        arg.openValue +
        '<br/>' +
        'Close: ' +
        arg.closeValue +
        '<br/>' +
        'High: ' +
        arg.highValue +
        '<br/>' +
        'Low: ' +
        arg.lowValue +
        '<br/>'
    };
  }
}
