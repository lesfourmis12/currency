import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { apiToken } from '../env';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor() {}

  private _baseUrl: string = 'https://finnhub.io/api/v1/';

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const apiReq = req.clone({
      url: this._baseUrl + req.url,
      params: req.params.set('token', apiToken),
      // headers: req.headers.set('X-Finnhub-Token', 'bulhopn48v6p4m01rod0'),
    });

    return next.handle(apiReq);
  }
}
