import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(this._handleError));
  }

  private _handleError(err: HttpErrorResponse) {
    let errorMsg: string = '';
    if (err.error instanceof ErrorEvent) {
      // client errors
      console.log('client error');
      errorMsg = `Error: ${err.error.message}`;
    } else {
      // server errors
      console.log('server error');
      errorMsg = `Server error, Code: ${err.status}, Message: ${err.message}`;
    }
    return throwError(errorMsg);
  }
}
