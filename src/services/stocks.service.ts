import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NotifierService } from './notifier.service';

export type Resolution = '1' | '5' | '15' | '30' | '60' | 'D' | 'W' | 'M' | 1 | 5 | 15 | 30 | 60; 

export interface Symbol {
  description: string,
  displaySymbol: string,
  symbol: string
}

@Injectable({
  providedIn: 'root',
})
export class StocksService {

  constructor(
    private _http: HttpClient,
    private _notifyService: NotifierService
  ) {}

  public async getStocks(): Promise<any> {
    const promise = new Promise<Symbol[]>(async (resolve, reject) => {
      let symbols: Symbol[] = [];

      try {
        const res: any = await this._http
          .get('forex/symbol?exchange=oanda')
          .toPromise();

        symbols = res;

      } catch (err) {
        this._notifyService.addMessage(err.message || 'Sever error', 'err');
      }

      resolve(symbols);
    });

    return promise;
  }

  public async getCandles(symbol: string, resolution: Resolution, from: Date, to: Date): Promise<any> {
    const promise = new Promise<any>(async (resolve, reject) => {
      const fromSecs = Math.floor(from.getTime() / 1000);
      const toSecs = Math.floor(to.getTime() / 1000);

      let candles = [];

      let params = {
        symbol,
        resolution: `${resolution}`,
        from: fromSecs.toString(),
        to: toSecs.toString()
      }

      try {
        const res: any = await this._http
          .get('stock/candle', { params })
          .toPromise();

          let mappedData = [];

          for (let i = 0; i < res['c'].length; i++) {
            mappedData.push({
              c: res['c'][i],
              o: res['o'][i],
              h: res['h'][i],
              l: res['l'][i],
              v: res['v'][i],
              t: new Date(res['t'][i]*1000)
            })
          }

          candles = mappedData;

      } catch (err) {
        this._notifyService.addMessage(err.message || 'Sever error', 'err');
      }

      resolve(candles);
    });

    return promise;
  }

  
  public async getCompanyProfile(symbol: string): Promise<any> {
    const promise = new Promise<any>(async (resolve, reject) => {

      let profile = {};

      let params = {
        symbol
      }

      try {
        const res: any = await this._http
          .get('stock/profile2', { params })
          .toPromise();

          console.log(res);
          profile = res;

      } catch (err) {
        this._notifyService.addMessage(err.message || 'Sever error', 'err');
      }

      resolve(profile);
    });

    return promise;
  }

  public getResList() {
    return [ '1', '5', '15', '30', '60', 'D', 'W', 'M' ]
  };
}
