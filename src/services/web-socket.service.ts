import { Injectable, OnDestroy, Inject } from '@angular/core';
import {
  Observable,
  SubscriptionLike,
  Subject,
  Observer,
  interval,
} from 'rxjs';
import {
  filter,
  map,
  share,
  distinctUntilChanged,
  takeWhile,
} from 'rxjs/operators';
import { WebSocketSubject, WebSocketSubjectConfig } from 'rxjs/webSocket';

import {
  IWebsocketService,
  IWsMessage,
  WebSocketConfig,
} from '../modules/web-socket/web-socket.interface';
import { config } from '../modules/web-socket/web-socket.config';

import { NotifierService } from './notifier.service';

@Injectable({
  providedIn: 'root',
})
export class WebSocketService implements IWebsocketService, OnDestroy {
  private config: WebSocketSubjectConfig<IWsMessage<any>>;

  private websocketSub: SubscriptionLike;
  private statusSub: SubscriptionLike;

  private reconnection$: Observable<number>;
  private websocket$: WebSocketSubject<IWsMessage<any>>;
  private connection$: Observer<boolean>;
  private wsMessages$: Subject<IWsMessage<any>>;

  private reconnectInterval: number;
  private reconnectAttempts: number;
  private isConnected: boolean;

  public status: Observable<boolean>;

  constructor(
    @Inject(config) private wsConfig: WebSocketConfig,
    private _notifier: NotifierService
  ) {
    this.wsMessages$ = new Subject<IWsMessage<any>>();

    this.reconnectInterval = wsConfig.reconnectInterval || 5000; // pause between reconnections
    this.reconnectAttempts = wsConfig.reconnectAttempts || 10; // number of connection attempts

    this.config = {
      url: wsConfig.url,
      closeObserver: {
        next: (event: CloseEvent) => {
          this.websocket$ = null;
          this.connection$.next(false);
        },
      },
      openObserver: {
        next: (event: Event) => {
          console.log('WebSocket connected!');
          this.connection$.next(true);
        },
      },
    };

    // connection status
    this.status = new Observable<boolean>((observer) => {
      this.connection$ = observer;
    }).pipe(share(), distinctUntilChanged());

    // run reconnect if not connection
    this.statusSub = this.status.subscribe((isConnected) => {
      this.isConnected = isConnected;

      if (
        !this.reconnection$ &&
        typeof isConnected === 'boolean' &&
        !isConnected
      ) {
        this.reconnect();
      }
    });

    this.websocketSub = this.wsMessages$.subscribe({
      next: null,
      error: (error: ErrorEvent) => {
        _notifier.addMessage('Connection error: ' + error.message, 'err');
      },
    });

    this.connect();
  }

  ngOnDestroy() {
    this.websocketSub.unsubscribe();
    this.statusSub.unsubscribe();
  }

  // connect to WebSocked
  private connect(): void {
    this.websocket$ = new WebSocketSubject(this.config);

    this.websocket$.subscribe(
      (message) => this.wsMessages$.next(message),
      (error: Event) => {
        if (!this.websocket$) {
          // run reconnect if errors
          this.reconnect();
        }
      }
    );
  }

  // reconnect if not connecting or errors
  private reconnect(): void {
    this.reconnection$ = interval(this.reconnectInterval).pipe(
      takeWhile(
        (v, index) => index < this.reconnectAttempts && !this.websocket$
      )
    );

    this.reconnection$.subscribe({
      next: () => {
        this.connect();
      },
      error: null,
      complete: () => {
        this.reconnection$ = null;

        // Subject complete if reconnect attemts ending
        if (!this.websocket$) {
          this.wsMessages$.complete();
          this.connection$.complete();
        }
      },
    });
  }

  // on message event
  public on<T>(event?: string): Observable<T> {
    //{"type":"ping"}
    //{"type":"trade", data":[{"p":20.401139999999998,"s":"OANDA:USD_MXN","t":1605304800017,"v":0}]}
    //{"type": "error", msg: "Malformed message"}
    if (event) {
      return this.wsMessages$.pipe(
        filter((message: IWsMessage<T>) => message.type === event),
        map((message: IWsMessage<T>) => message.data)
      );
    } else {
      return this.wsMessages$.pipe(
        map((message: any) => message)
      );
    }
  }

  // on message to server
  public send(event: string, data: any = {}): void {
    if (event && this.isConnected) {
      // event example "subscribe" | "trade" | "error" | "ping"
      let message: IWsMessage<any> = {type: event, ...data};
      console.log('send to server ' + JSON.stringify(message));
      this.websocket$.next(message);
    } else {
      console.log('Socket connection error!');
    }
  }
}
