import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export interface Alert {
  id: number;
  caption: string;
  status: 'ok' | 'err';
}

@Injectable({
  providedIn: 'root',
})
export class NotifierService {
  private _id: number = 0;
  private _alerts: Alert[] = [];
  readonly alerts$: Subject<Alert[]> = new Subject();

  constructor() {}

  public addMessage(message: string, status: 'ok' | 'err' = 'ok') {
    this._alerts.push({ id: this._id, caption: message, status });
    this._alerts = this._alerts.slice(-5);
    this.alerts$.next(this._alerts);

    const setTimer = (id: number) => {
      setTimeout(() => {
        this._alerts = this._alerts.filter((alert) => alert.id != id);
        this.alerts$.next(this._alerts);
      }, 5000);
    };

    setTimer(this._id++);
  }
}
