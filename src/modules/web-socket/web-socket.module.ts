import { NgModule, ModuleWithProviders, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebSocketConfig } from './web-socket.interface';
import { WebSocketService } from '../../services/web-socket.service';

import { config } from './web-socket.config'

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [WebSocketService],
})
export class WebSocketModule {
  public static config(wsConfig: WebSocketConfig): ModuleWithProviders<WebSocketModule> {
    return {
      ngModule: WebSocketModule,
      providers: [
        { provide: config, useValue: wsConfig }
      ],
    };
  }
}
